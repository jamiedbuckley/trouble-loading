﻿using UnityEngine;
using System.Collections;

public class PowerUps : MonoBehaviour {
	
	//General Variables
	public GameObject player;
	public GameObject playerRenderer;
	private Color tempColour;
	private bool fadeIn;
	private float fadeTime;
	public Material reflectiveBumpSpec;
	public Material transparentBumpSpec;
	
	//PowerUp Variables
	[HideInInspector]
	public float timer;
	public float invincibilityLifetime;
	public float speedLifetime;
	public float invertLifetime;
	[HideInInspector]
	public int randomNumber;
	[HideInInspector]
	public bool justSpawned;
	private float spawnerCooldown;
	private float spawnerTimer;
	private bool powerIsActive;
	private bool speedChange;
	
	//enum  for the powerups
	public enum powerUps{
		none,
		Invincibility,
		InvertControls,
		FlipLevel,
		Speed,
		Slow,
	}	
	[HideInInspector]
	public powerUps Powerups = powerUps.none;

	// Use this for initialization
	private void Start () {
		
		//Find the shaders on the first frame for fading
		//reflectiveBumpSpec = Shader.Find("Reflective/Bumped Specular");
		//transparentBumpSpec =  Shader.Find("Transparent/Bumped Specular");
		//Set fade speed
		fadeTime = 5;
		
		//Cooldown of the spawn rate
		spawnerCooldown = 10;

	}
	
	// Update is called once per frame
	private void Update () {
		PowerUpBehaviours();
		PowerUpSpawning();
	}
	
	private void PowerUpSpawning(){
		randomNumber = Random.Range(0,2);
		
		if(justSpawned && !powerIsActive){
			spawnerTimer += Time.deltaTime;
			if(spawnerTimer >= spawnerCooldown){
				spawnerTimer = 0;
				randomNumber = 0;
				justSpawned = false;
			}
		}
	}
	
	
	private void PowerUpBehaviours(){
		//Begin the invincible exicution
		if(Powerups == powerUps.Invincibility){
			timer += Time.deltaTime;
			if(timer >= invincibilityLifetime){
				Powerups = powerUps.none;
				timer = 0;
			}
			
			powerIsActive = true;
			
			//Fade the character in and out to show invincibility
			tempColour = playerRenderer.renderer.material.color;
			playerRenderer.renderer.material = transparentBumpSpec;
			
			if(tempColour.a < 1 && !fadeIn){
				tempColour.a += fadeTime * Time.deltaTime;
			}else{
				fadeIn = true;	
			}
				if(tempColour.a > 0 && fadeIn){
					tempColour.a -= fadeTime * Time.deltaTime;	
				}else{
					fadeIn = false;	
			}
			
			playerRenderer.renderer.material.color = tempColour;
			player.GetComponent<CharacterControl>().cantDie = true;
			
		}else{
			//Set everything back to original state
			playerRenderer.renderer.material = reflectiveBumpSpec;
			tempColour = playerRenderer.renderer.material.color;
			tempColour.a = 1;
			playerRenderer.renderer.material.color = tempColour;
			powerIsActive = false;
			player.GetComponent<CharacterControl>().cantDie = false;	
		}

		if(Powerups == powerUps.InvertControls){
			Debug.Log ("Debug 3 invert");
			timer += Time.deltaTime;
			if(timer >= invertLifetime){
				Debug.Log ("turn invert off");
				Powerups = powerUps.none;
				timer = 0;
			}
			Camera.main.GetComponent<Control>().invertedControls = true;
		}
		else{
			Camera.main.GetComponent<Control>().invertedControls = false;
		}
		
		if(Powerups == powerUps.FlipLevel){
			Camera.main.GetComponent<Control>().levelFlip = true;
			Powerups = powerUps.none;
		}
		
		if(Powerups == powerUps.Speed){
			Debug.Log("SPEEEEEEDY");
			timer += Time.deltaTime;
			if(timer >= speedLifetime){
				Powerups = powerUps.none;
				speedChange = true;
				timer = 0;
			}
			powerIsActive = true;
			player.GetComponent<CharacterControl>().speedUp = true;
			
		}
		else{
			if(speedChange){
				powerIsActive = false;
				player.GetComponent<CharacterControl>().slowDown = true;
				speedChange = false;
			}
	}
}
}
