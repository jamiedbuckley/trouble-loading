﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {
	
	[HideInInspector]
	public bool isCollected;
	
	private Vector3 collectedPosition;
	private bool addedScore;
	
	// Use this for initialization
	private void Start () {
	
		collectedPosition = GameObject.FindGameObjectWithTag("CollectableTarget").transform.position;
	}
	
	// Update is called once per frame
	private void Update () {
		
		CollectableBehaviour();
	}
	
	private void CollectableBehaviour(){
		if(isCollected){
			
			transform.parent = null;
			transform.collider.enabled = false;
			
			if(!addedScore){
				Camera.main.GetComponent<HUD>().addCollectable();
				addedScore = true;
			}
			
			transform.position = Vector3.MoveTowards(transform.position,
				collectedPosition,20 * Time.deltaTime);
			
			if(transform.position == collectedPosition){
				Destroy(gameObject);	
			}
		}
	}
}
