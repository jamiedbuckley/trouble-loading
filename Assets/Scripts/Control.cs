﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour {
	
	//General Variables
	private GameObject levelParent;
	private bool keyToggle;
	public GameObject playerModel;
	[HideInInspector]
	public bool lockControls;
	
	//Level Rotation Variables
	private float spinAngle;
	private int statesCount;
	private int rotationSpeed;
	private Vector3 tempRotation;
	[HideInInspector]
	public bool spinningLevel;
	[HideInInspector]
	public float currentSpinAngle;
	
	//Corner section control variables
	[HideInInspector]
	public bool canTurn;
	[HideInInspector]
	public GameObject currenthitobj;
	private bool turnNow;
	private bool beginCounting;
	private float turnTimer;
	private bool turnLeft;
	public AudioSource rotationSound;

	//Powerup Variables
	[HideInInspector]
	public bool invertedControls;
	[HideInInspector]
	public bool levelFlip;

	//enum  for the level rotation states
	enum levelStates{
		straight,
		left,
		up,
		right,
	}	
	levelStates states = levelStates.straight;
	
	//enum for the spin direction
	enum spinStates{
		none,
		left,
		right,
	}
	spinStates spinningStates = spinStates.none;
	
	//enum for the games overall playing states
	public enum gameStates{
		MainMenu,
		Playing,
		Dead,
	}
	[HideInInspector]
	public gameStates GameStates = gameStates.MainMenu;

	//Touch control variables 
	private RaycastHit hit;
	private Ray ray;
	
	public enum touchState{
		none,
		leftTouch,
		rightTouch,
	} 

	touchState touchingStates = touchState.none;

	// Use this for initialization
	private void Start () {
		
		rotationSpeed = 150;
	}
	
	// Update is called once per frame
	private void Update () {
		
		PlayerControl();
	}
	
	private void PlayerControl(){
		
		//Find the chunks parent so rotation can be assigned
		if(levelParent == null){
			levelParent = GameObject.FindGameObjectWithTag("levelChunks");
		}
		
		if(GameStates == gameStates.MainMenu){
			if(Input.anyKey){
				GameStates = gameStates.Playing;
			}
		}

		//If no touch detected on screen, set touch state to none
		if(Input.GetMouseButtonUp(0)){
			touchingStates = touchState.none;
		}
		
		if(GameStates == gameStates.Playing){
			//Touch Controls detection
			if (Input.GetMouseButtonDown(0)){
				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray, out hit, 100.0f)){
					if(hit.collider.tag == "RightTouch"){
						touchingStates = touchState.rightTouch;
					}else if(hit.collider.tag == "LeftTouch"){
						touchingStates = touchState.leftTouch;
					}
				}
			}

			//Take player input to rotate the levels
			if(!spinningLevel){
				if(!invertedControls){
					//Check for left input and execute the movement
					if(!keyToggle && Input.GetKeyDown(KeyCode.RightArrow) || touchingStates == touchState.rightTouch || Input.GetButtonDown("Xbox_RB")){
							if(!canTurn && !lockControls){
								playerModel.GetComponent<CharacterControl>().jumping = true;
								spinningLevel = true;
								spinningStates = spinStates.left;
								if(states == levelStates.straight && spinningStates == spinStates.left){
									currentSpinAngle = 0;
								}
								if(statesCount < 3){
									statesCount ++;
								}else{
									statesCount = 0;	
								}
								keyToggle = true;
							}else if(canTurn){
								
								beginCounting = true;
								turnLeft = false;
							}
						}else{
							keyToggle = false;	
					}
				

					//Check for right input and execute the movement
					if (!keyToggle && Input.GetKeyDown(KeyCode.LeftArrow) || touchingStates == touchState.leftTouch || Input.GetButtonDown("Xbox_LB")){
						if(!canTurn && !lockControls){
								playerModel.GetComponent<CharacterControl>().jumping = true;
								spinningLevel = true;
								spinningStates = spinStates.right;
								if(states == levelStates.straight && spinningStates == spinStates.right){
									currentSpinAngle = 360;
								}
								if(statesCount > 0){
									statesCount --;
									}else{
										statesCount = 3;	
									}
								keyToggle = true;
							}else if(canTurn){
							
								beginCounting = true;
								turnLeft = true;
							}
						}else{
							keyToggle = false;	
						}
					
					//Check for jump input and tell player to jump
					if (!keyToggle && Input.GetKeyDown(KeyCode.UpArrow) || Input.GetButtonDown("Xbox_A")){
							if(playerModel.GetComponent<CharacterControl>().onGround == true){
								playerModel.GetComponent<CharacterControl>().jumping = true;
								keyToggle = true;
							}
						}else{
						keyToggle = false;	
					}
					
					//Check input being held for sliding ability
					if(Input.GetKey(KeyCode.DownArrow) || Input.GetAxis("Xbox_leftStickY") == 1){
						playerModel.GetComponent<CharacterControl>().sliding = true;
					}else{
						playerModel.GetComponent<CharacterControl>().sliding = false;
					}
				}
				else{
					//Check for left input and execute the movement
					if(!keyToggle && Input.GetKeyDown(KeyCode.LeftArrow) || touchingStates == touchState.leftTouch || Input.GetButtonDown("Xbox_LB")){
						if(!canTurn && !lockControls){
							playerModel.GetComponent<CharacterControl>().jumping = true;
							spinningLevel = true;
							spinningStates = spinStates.left;
							if(states == levelStates.straight && spinningStates == spinStates.left){
								currentSpinAngle = 0;
							}
							if(statesCount < 3){
								statesCount ++;
							}else{
								statesCount = 0;	
							}
							keyToggle = true;
						}else if(canTurn){
							
							beginCounting = true;
							turnLeft = false;
						}
					}else{
						keyToggle = false;	
					}
					
					
					//Check for right input and execute the movement
					if (!keyToggle && Input.GetKeyDown(KeyCode.RightArrow) || touchingStates == touchState.rightTouch || Input.GetButtonDown("Xbox_RB")){
						if(!canTurn && !lockControls){
							playerModel.GetComponent<CharacterControl>().jumping = true;
							spinningLevel = true;
							spinningStates = spinStates.right;
							if(states == levelStates.straight && spinningStates == spinStates.right){
								currentSpinAngle = 360;
							}
							if(statesCount > 0){
								statesCount --;
							}else{
								statesCount = 3;	
							}
							keyToggle = true;
						}else if(canTurn){
							
							beginCounting = true;
							turnLeft = true;
						}
					}else{
						keyToggle = false;	
					}
					
					//Check for jump input and tell player to jump
					if (!keyToggle && Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxis("Xbox_leftStickY") == 1){
						if(playerModel.GetComponent<CharacterControl>().onGround == true){
							playerModel.GetComponent<CharacterControl>().jumping = true;
							keyToggle = true;
						}
					}else{
						keyToggle = false;	
					}
					
					//Check input being held for sliding ability
					if(Input.GetKey(KeyCode.UpArrow) || Input.GetButton("Xbox_A")){
						playerModel.GetComponent<CharacterControl>().sliding = true;
					}else{
						playerModel.GetComponent<CharacterControl>().sliding = false;
					}
				}

				if(levelFlip){
					rotationSpeed = 300;
					if(!canTurn && !lockControls){
						playerModel.GetComponent<CharacterControl>().jumping = true;
						spinningLevel = true;
						spinningStates = spinStates.left;
						if(states == levelStates.straight && spinningStates == spinStates.left){
							currentSpinAngle = 0;
						}
						switch(statesCount){
						case 0:
							statesCount = 2;
							break;
						case 1:
							statesCount = 3;
							break;
						case 2:
							statesCount = 0;
							break;
						case 3:
							statesCount = 1;
							break;
							
						}
						keyToggle = true;
					}else if(canTurn){
						beginCounting = true;
						turnLeft = false;
					}	
				}
				else{
					rotationSpeed = 150;
				}

				//If in the corner section take input and create a timer
				if(beginCounting){
					turnTimer += Time.deltaTime;
					if(turnTimer > 0.3f){
						turnNow = false;
						turnTimer = 0;
						beginCounting = false;
					}
					turnNow = true;
					if(turnNow){
						if(currenthitobj != null){
							if(turnLeft){
								currenthitobj.transform.parent.GetComponent<ChunkBehaviour>().cornerSpinLeft = true;
							}else{
								currenthitobj.transform.parent.GetComponent<ChunkBehaviour>().cornerSpinRight = true;
							}
						}
					}
				}
			}
	
			//Controls the spinning of the level to different sides
			if(spinningLevel){
			
				//Assigns the state depending on the int variable
				if(statesCount == 0){
					states = levelStates.straight;
				}else if(statesCount == 1){
					states = levelStates.left;
				}else if(statesCount == 2){
					states = levelStates.up;
				}else if(statesCount == 3){
					states = levelStates.right;
				}
				
				//Assign the target spin value for the level
				if(states == levelStates.left)
					spinAngle = 90;
					if(states == levelStates.up)
						spinAngle = 180;
						if(states == levelStates.right)
							spinAngle = 270;
							if(states == levelStates.straight && spinningStates == spinStates.left){
								spinAngle = 360;
				}else if(states == levelStates.straight && spinningStates == spinStates.right){
					spinAngle = 0;
				}
				
				//Rotate the level depending on the amount of current angle
				if(spinningStates == spinStates.left){
					if(currentSpinAngle < spinAngle){
						currentSpinAngle += rotationSpeed * Time.deltaTime;
						currentSpinAngle = Mathf.Clamp(currentSpinAngle,spinAngle - 90,spinAngle);
					}else{
						spinningLevel = false;
						rotationSound.Stop();
						if(!rotationSound.isPlaying){
							rotationSound.Play();
						}
						Camera.main.GetComponent<HUD>().triggered = true;
					}
				}else if(spinningStates == spinStates.right){
					if(currentSpinAngle > spinAngle){
						currentSpinAngle -= rotationSpeed * Time.deltaTime;
						currentSpinAngle = Mathf.Clamp(currentSpinAngle,spinAngle,spinAngle + 90);
					}else{
						spinningLevel = false;
						rotationSound.Stop();
							if(!rotationSound.isPlaying){
							rotationSound.Play();
						}
						Camera.main.GetComponent<HUD>().triggered = true;
					}
				}
				levelFlip = false;
			}
		}
	}
}
