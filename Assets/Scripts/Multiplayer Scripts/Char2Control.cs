﻿using UnityEngine;
using System.Collections;

public class Char2Control : MonoBehaviour {
	
	//General Variables
	private GameObject levelParent;
	private bool keyToggle;
	public GameObject playerModel;
	[HideInInspector]
	public bool lockControls;
	
	//Level Rotation Variables
	private float spinAngle;
	private int statesCount;
	private int rotationSpeed;
	private Vector3 tempRotation;
	[HideInInspector]
	public bool spiningLevel;
	[HideInInspector]
	public float currentSpinAngle;
	
	//Corner section control variables
	[HideInInspector]
	public bool canTurn;
	[HideInInspector]
	public GameObject currenthitobj;
	private bool turnNow;
	private bool beginCounting;
	private float turnTimer;
	private bool turnLeft;
	public AudioSource rotationSound;
	
	//enum  for the level rotation states
	enum levelStates{
		straight,
		left,
		up,
		right,
	}	
	levelStates states = levelStates.straight;
	
	//enum for the spin direction
	enum spinStates{
		none,
		left,
		right,
	}
	spinStates spinningStates = spinStates.none;
	
	//enum for the games overall playing states
	public enum gameStates{
		MainMenu,
		Playing,
		Dead,
	}
	[HideInInspector]
	public gameStates GameStates = gameStates.MainMenu;
	
	// Use this for initialization
	private void Start () {
		
		rotationSpeed = 150;
	}
	
	// Update is called once per frame
	private void Update () {
		
		PlayerControl();
	}
	
	private void PlayerControl(){
		
		//Find the chunks parent so rotation can be assigned
		if(levelParent == null){
			levelParent = GameObject.FindGameObjectWithTag("levelChunks");
		}
		
		if(GameStates == gameStates.MainMenu){
			if(Input.anyKey){
				GameStates = gameStates.Playing;
			}
		}

		
		if(GameStates == gameStates.Playing){
			//Take player input to rotate the levels
			if(!spiningLevel){
				//Check for left input and exicute the movement
				if(!keyToggle && Input.GetKeyDown(KeyCode.D) || Input.GetButtonDown("Xbox_RB")){
					if(!canTurn && !lockControls){
						playerModel.GetComponent<CharacterControl>().jumping = true;
						spiningLevel = true;
						spinningStates = spinStates.left;
						if(states == levelStates.straight && spinningStates == spinStates.left){
							currentSpinAngle = 0;
						}
						if(statesCount < 3){
							statesCount ++;
						}else{
							statesCount = 0;	
						}
						keyToggle = true;
					}else if(canTurn){
						
						beginCounting = true;
						turnLeft = false;
					}
				}else{
					keyToggle = false;	
				}
				
				//Check for right input and exicute the movement
				if (!keyToggle && Input.GetKeyDown(KeyCode.A) || Input.GetButtonDown("Xbox_LB")){
					if(!canTurn && !lockControls){
						playerModel.GetComponent<CharacterControl>().jumping = true;
						spiningLevel = true;
						spinningStates = spinStates.right;
						if(states == levelStates.straight && spinningStates == spinStates.right){
							currentSpinAngle = 360;
						}
						if(statesCount > 0){
							statesCount --;
						}else{
							statesCount = 3;	
						}
						keyToggle = true;
					}else if(canTurn){
						
						beginCounting = true;
						turnLeft = true;
					}
				}else{
					keyToggle = false;	
				}
				
				//If in the corner section take input and create a timer
				if(beginCounting){
					turnTimer += Time.deltaTime;
					if(turnTimer > 0.3f){
						turnNow = false;
						turnTimer = 0;
						beginCounting = false;
					}
					turnNow = true;
					if(turnNow){
						if(currenthitobj != null){
							if(turnLeft){
								currenthitobj.transform.parent.GetComponent<ChunkBehaviour>().cornerSpinLeft = true;
							}else{
								currenthitobj.transform.parent.GetComponent<ChunkBehaviour>().cornerSpinRight = true;
							}
						}
					}
				}
				
				//Check for jump input and tell player to jump
				if (!keyToggle && Input.GetKeyDown(KeyCode.W) || Input.GetButtonDown("Xbox_A")){
					if(playerModel.GetComponent<CharacterControl>().onGround == true){
						playerModel.GetComponent<CharacterControl>().jumping = true;
						keyToggle = true;
					}
				}else{
					keyToggle = false;	
				}
				
				//Check input being held for sliding ability
				if(Input.GetKey(KeyCode.S) || Input.GetAxis("Xbox_leftStickY") == 1){
					playerModel.GetComponent<CharacterControl>().sliding = true;
				}else{
					playerModel.GetComponent<CharacterControl>().sliding = false;
				}
			}
			
			//Controls the spinning of the level to different sides
			if(spiningLevel){
				
				//Assigns the state depending on the int variable
				if(statesCount == 0){
					states = levelStates.straight;
				}else if(statesCount == 1){
					states = levelStates.left;
				}else if(statesCount == 2){
					states = levelStates.up;
				}else if(statesCount == 3){
					states = levelStates.right;
				}
				
				//Assign the target spin value for the level
				if(states == levelStates.left)
					spinAngle = 90;
				if(states == levelStates.up)
					spinAngle = 180;
				if(states == levelStates.right)
					spinAngle = 270;
				if(states == levelStates.straight && spinningStates == spinStates.left){
					spinAngle = 360;
				}else if(states == levelStates.straight && spinningStates == spinStates.right){
					spinAngle = 0;
				}
				
				//Rotate the level depending on the amount of current angle
				if(spinningStates == spinStates.left){
					if(currentSpinAngle < spinAngle){
						currentSpinAngle += rotationSpeed * Time.deltaTime;
						currentSpinAngle = Mathf.Clamp(currentSpinAngle,spinAngle - 90,spinAngle);
					}else{
						spiningLevel = false;
						rotationSound.Stop();
						if(!rotationSound.isPlaying){
							rotationSound.Play();
						}
						///////////////CHANGE FOR PLAYER 2 CAMERA SHAKE
						//Camera.main.GetComponent<HUD>().triggered = true;
					}
				}else if(spinningStates == spinStates.right){
					if(currentSpinAngle > spinAngle){
						currentSpinAngle -= rotationSpeed * Time.deltaTime;
						currentSpinAngle = Mathf.Clamp(currentSpinAngle,spinAngle,spinAngle + 90);
					}else{
						spiningLevel = false;
						rotationSound.Stop();
						if(!rotationSound.isPlaying){
							rotationSound.Play();
						}
						///////////////CHANGE FOR PLAYER 2 CAMERA SHAKE
						//Camera.main.GetComponent<HUD>().triggered = true;
					}
				}
			}
		}
	}
}
