﻿using UnityEngine;
using System.Collections;

public class CharacterControl : MonoBehaviour {
	
	//Ground detection/gravity variables
	private RaycastHit groundhitPoint;
	private float jumpMaxHeight;
	private float jumpMinHeight;
	private float fallSpeed;
	private float jumpSpeed;
	private float fallDelay;
	[HideInInspector]
	public bool jumping;
	[HideInInspector]
	public bool sliding;
	[HideInInspector]
	public bool onGround;
	
	//Variables for temp sliding. Will need changing later
	public GameObject playerRenderer;
	public GameObject slidingModel;
	public Collider slidingCollider;
	public Collider runningCollider;

	//Variables for finding the player for animation
	public GameObject playerAnim;

	//Variables for player two
	public bool isPlayer2;
	public GameObject secondCamera;
	
	//Variables for other collisions
	private RaycastHit feethitPoint;
	
	//Powerup Variables
	[HideInInspector]
	public bool cantDie;
	[HideInInspector]
	public bool speedUp;
	[HideInInspector]
	public bool slowDown;

	// Use this for initialization
	private void Start () {
		
		fallSpeed = 5;
		jumpSpeed = 5;
		onGround = true;
		playerRenderer.SetActive(true);
	}
	
	// Update is called once per frame
	private void Update () {
		playerBehaviour();
	}
	
	private void playerBehaviour(){
	
	if(Camera.main.GetComponent<Control>().GameStates == Control.gameStates.Playing){
		//Raycast to detect the ground collision
		if (Physics.Raycast (transform.position, -Vector3.up,out groundhitPoint)) {
				
			}
			
		//Raycast to detect feet collisions
		if (Physics.Raycast (transform.position, Vector3.forward,out feethitPoint)) {
				
				//TEMPORARY UNTIL REAL MODEL
				if(feethitPoint.collider.name == "TempWallCollider"){
					if(feethitPoint.distance <= 0.5f){
						Application.LoadLevel(Application.loadedLevelName);	
					}
				}
				
				Debug.DrawRay(new Vector3(transform.position.x,
					transform.position.y,transform.position.z),Vector3.forward);
				
			}

			if(speedUp){
				Camera.main.GetComponent<LevelCreation>().levelSpeed = 25;
				speedUp = false;
			};

			if(slowDown){
				Camera.main.GetComponent<LevelCreation>().levelSpeed = 20;
				slowDown = false;
			};

			//Find the lowest point below character and set it as fall point(For Dynamic landing uncomment hitpoint)
			jumpMinHeight = -3;//groundhitPoint.point.y;
			
			//Here the jump/gravity is executed depending on the raycast/max height
			if(!jumping){
				transform.position = Vector3.MoveTowards(transform.position,new Vector3(groundhitPoint.point.x,
					jumpMinHeight,groundhitPoint.point.z),
					fallSpeed * Time.deltaTime);
				}else{
					transform.position = Vector3.MoveTowards(transform.position,new Vector3(groundhitPoint.point.x,
						jumpMaxHeight,groundhitPoint.point.z),
							jumpSpeed * Time.deltaTime);
					onGround = false;
					//Temporary jumping animation >>>>>>>>>>>REMOVE OR ALTER WHEN NEW MODEL IS ADDED<<<<<<<<<<<
					playerAnim.GetComponent<Animation>().animation.Play("jumpNew");
					playerAnim.GetComponent<Animation>().animation["jumpNew"].speed = 0.8f;
			}
			
			//Sliding code is executed here
			if(sliding){
				//TEMP SLIDING CODE <<<SWITCHING MODEL UNTIL REAL ANIMATION IS PROVIDED>>>
				playerRenderer.SetActive(false);
				slidingModel.SetActive(true);
				slidingCollider.enabled = true;
				runningCollider.enabled = false;
			}else{
				playerRenderer.SetActive(true);
				slidingModel.SetActive(false);
				slidingCollider.enabled = false;
				runningCollider.enabled = true;
			}
				
				//Once at the top of the jump apply a fall delay, so that it looks smooth,
				//then make the player fall down
				if(transform.position.y == jumpMaxHeight){
					fallDelay += Time.deltaTime;
					if(fallDelay >= 0.1f){
						jumping = false;
						fallDelay = 0;
					}
				}else if(transform.position.y == jumpMinHeight){
					playerAnim.GetComponent<Animation>().animation.Play("RunNEw");
					onGround = true;
				}
			
			//Calls once to set the position of the max jump on any surface/model 
			if(jumpMaxHeight == 0){
				jumpMaxHeight = transform.position.y + 1.2f;	
			}
		}
	}
	
	//If player enters a trigger, code executed here
	private void OnTriggerEnter(Collider collider){
		if(collider.collider.tag == "DoorTrigger"){
			collider.collider.gameObject.transform.parent.GetComponent<ChunkBehaviour>().doorTriggered = true;
		}else if(collider.collider.tag == "CornerTrigger"){
			if(!isPlayer2){
				Camera.main.GetComponent<Control>().currenthitobj = collider.collider.gameObject;
				Camera.main.GetComponent<LevelCreation>().releaseAngles = true;
			}else{
				secondCamera.GetComponent<Char2Control>().currenthitobj = collider.collider.gameObject;
				secondCamera.GetComponent<MPLevelCreation>().releaseAngles = true;
			}
		}else if(collider.collider.tag == "ControlsDisabled"){
			if(!isPlayer2){
				Camera.main.GetComponent<Control>().lockControls = true;
				Camera.main.GetComponent<Control>().canTurn = true;
			}else{
				secondCamera.GetComponent<Char2Control>().lockControls = true;
				secondCamera.GetComponent<Char2Control>().canTurn = true;
			}
		}else if(collider.collider.tag == "ControlsEnabled"){
			//Camera.main.GetComponent<Control>().lockControls = false;
		}else if(collider.collider.tag == "InstaKillObject" && !cantDie){
			Application.LoadLevel(Application.loadedLevelName);
		}else if(collider.collider.tag == "Collectable"){
			collider.gameObject.transform.GetComponent<Collectable>().isCollected = true;
		}else if(collider.collider.tag == "PowerUp"){
			int randomNumber = Random.Range(1,5);
			Debug.Log("Random generation:  " + randomNumber);
			if(!isPlayer2){
				switch(randomNumber){
				case 1:
					Camera.main.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.Invincibility;
					Debug.Log("Invincible");	
					break;
				case 2:
					Camera.main.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.FlipLevel;
					Debug.Log("Flip Level");
					break;
				case 3:
					Camera.main.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.Speed;
					Debug.Log("Speed");
					break;
				case 4:
					Camera.main.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.InvertControls;
					Debug.Log("Controls Inverted");
					break;
				}
				Camera.main.GetComponent<PowerUps>().timer = 0;
			}else{
				switch(randomNumber){
				case 1:
					secondCamera.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.Invincibility;
					break;
				case 2:
					secondCamera.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.FlipLevel;
					break;
				case 3:
					secondCamera.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.Speed;
					break;
				case 4:
					secondCamera.GetComponent<PowerUps>().Powerups = PowerUps.powerUps.InvertControls;
					break;
				}
				secondCamera.GetComponent<PowerUps>().timer = 0;
			}
			collider.gameObject.SetActive(false);
		}
	}
	
	private void OnTriggerExit(Collider collider){
		if(collider.collider.tag == "CornerTrigger"){
			if(!isPlayer2){
				Camera.main.GetComponent<Control>().canTurn = false;
			}else{
				secondCamera.GetComponent<Char2Control>().canTurn = false;
			}
		}
	}
}
