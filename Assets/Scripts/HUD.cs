﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {
	
	//HUD display text Variables
	public TextMesh distanceText;
	public TextMesh scoreText;
	public TextMesh startingText;
	public TextMesh collectedText;
	
	//Score Variables
	private float distancePerMeter;
	private int distanceTravelled;
	private int amountCollected;
	private int totalScore;
	private int collectableBonus;
	
	//General HUD Variables
	public GameObject lockImage;
	public Texture2D lockShut;
	private Color fadeColour;
	private bool imageFadeIn;
	
	//Screen shaking variables <<<MAY NEED REMOVAL IF NOT BEING USED>>>
	private int randomDirection;
	private Vector3 cameraStartPos;
	private float randomX;
	private float randomY;
	private float timer;
	[HideInInspector]
	public bool triggered;
	
	// Use this for initialization
	private void Start () {
		
		lockImage.SetActive(false);
		startingText.renderer.enabled = true;
		fadeColour = lockImage.renderer.material.color;
		fadeColour.a = 0;
		lockImage.renderer.material.color = fadeColour;
		
		cameraStartPos = transform.position;
	}
	
	// Update is called once per frame
	private void Update () {
		
		CameraShake();
		Scores();
		HudObjects();
	}
	
	private void HudObjects(){
		//Handels the starting text, choose if its displayed or not
		if(Camera.main.GetComponent<Control>().GameStates == Control.gameStates.Playing){
			startingText.renderer.enabled = false;
		}
		
		//Handles Flashing the locked controls image
		if(Camera.main.GetComponent<Control>().lockControls == true){
			lockImage.SetActive(true);
			
			if(fadeColour.a < 1 && !imageFadeIn){
				fadeColour.a += 8 * Time.deltaTime;
			}else{
				imageFadeIn = true;	
			}
				if(fadeColour.a > 0 && imageFadeIn){
					fadeColour.a -= 8 * Time.deltaTime;	
				}else{
					imageFadeIn = false;	
				}
					lockImage.renderer.material.color = fadeColour;
					lockImage.renderer.material.mainTexture = lockShut;
		}else{
			lockImage.SetActive(false);
		}
	}
	
	private void Scores(){
		
		//Calculate the distance per meter travelled and at it to the variable & round to next int
		//Distance travelled is increased by the level speed
		if(Camera.main.GetComponent<Control>().GameStates == Control.gameStates.Playing){
			
			distanceText.renderer.enabled = true;
			scoreText.renderer.enabled = true;
			collectedText.transform.gameObject.SetActive(true);
			
			if(distancePerMeter < 1){
						distancePerMeter += Camera.main.GetComponent<LevelCreation>().levelSpeed * Time.deltaTime;
					}else{
						distanceTravelled += (int)distancePerMeter;
						distancePerMeter = 0;	
			}
			
			//Calculate the total score
			totalScore = distanceTravelled + collectableBonus; 
			
			//Output text for the distance travelled
			distanceText.text = "Distance: " + distanceTravelled + "m";
			//Output text for the total score (distance + collectables + multipliers)
			scoreText.text = "Score: " + totalScore;
			//Output for the collected amount of collectables
			collectedText.text = "X" + amountCollected; 
			
		}else{
			distanceText.renderer.enabled = false;
			scoreText.renderer.enabled = false;
			collectedText.transform.gameObject.SetActive(false);
		}
	}
	
	//Method to increment score from other classes
	public void addCollectable(){
			amountCollected++;
			collectableBonus += 100;
	}
	
	private void CameraShake(){
		
		//Possible Screen shaking, needs feedback to see if fully implemented
		//If kept move out of update into another function
		if(triggered){
		timer += Time.deltaTime;
			if(timer < 0.3f){
				randomX = Random.Range(0,0.15f);
				randomY = Random.Range(0,0.15f);
				randomDirection = Random.Range(0,1);
				
				if(randomDirection == 1){
					transform.position = Vector3.MoveTowards(transform.position,
						new Vector3(cameraStartPos.x + randomX,cameraStartPos.y + randomY,
						cameraStartPos.z), 20 * Time.deltaTime);
				}else{
					transform.position = Vector3.MoveTowards(transform.position,
						new Vector3(cameraStartPos.x - randomX,cameraStartPos.y - randomY,
						cameraStartPos.z), 20 * Time.deltaTime);
				}
			}else{
				transform.position = cameraStartPos;
				timer = 0;
				triggered = false;
			}
		}
	}
}
