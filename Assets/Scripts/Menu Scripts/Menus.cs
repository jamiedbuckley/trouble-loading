﻿using UnityEngine;
using System.Collections;

public class Menus : MonoBehaviour {

	//Menu Click control variables 
	private RaycastHit hit;
	private Ray ray;
	private bool toggle;

	// Use this for initialization
	private void Start () {
	
	}
	
	// Update is called once per frame
	private void Update () {
		MenuBehaviour();
	}

	private void MenuBehaviour(){
	//Click detection
	if(Input.GetMouseButtonDown(0)){
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		}
		if(Input.GetMouseButtonUp(0)){
			if (Physics.Raycast (ray, out hit, 100.0f)){
				if(hit.collider.name == "SPText"){
					Application.LoadLevel("SPGameScene");
				}else if(hit.collider.name == "MPText"){
					Application.LoadLevel("MPGameScene");
				}
			}
		}
	}
}
