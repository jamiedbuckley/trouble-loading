﻿using UnityEngine;
using System.Collections;

public class ChunkBehaviour : MonoBehaviour {
		
	//Door specific Variables
	public bool hasDoor;
	public GameObject doorTrigger;
	public GameObject leftDoor;
	public GameObject rightDoor;
	public float leftDoorValue;
	public float rightDoorValue;
	[HideInInspector]
	public bool doorTriggered;
	
	//Corner Chunk Specific Variables
	public bool isCorner;
	public bool isLeft;
	public bool isRight;
	[HideInInspector]
	public bool cornerSpinLeft;
	[HideInInspector]
	public bool cornerSpinRight;
	private Vector3 tempSpinAngles;
	
	//Chunk Lights
	public bool hasLights;
	public GameObject light1;
	public GameObject light2;

	//Variables for texture switching
	[HideInInspector]
	public bool textureSet;
	private int randomNumber;
	
	// Use this for initialization
	private void Start () {

		randomNumber = Random.Range(0,3);
	}
	
	// Update is called once per frame
	private void Update () {
		
		ChunkSpecificAction();
	}
	
	//Temporary door animation code
	private void ChunkSpecificAction(){
		if(hasDoor){
			if(doorTriggered){
			leftDoor.transform.localPosition = Vector3.MoveTowards(leftDoor.transform.localPosition,
				new Vector3(leftDoor.transform.localPosition.x,leftDoor.transform.localPosition.y,leftDoorValue),
				3 * Time.deltaTime);
			
			rightDoor.transform.localPosition = Vector3.MoveTowards(rightDoor.transform.localPosition,
				new Vector3(rightDoor.transform.localPosition.x,rightDoor.transform.localPosition.y,rightDoorValue),
				3 * Time.deltaTime);

				if(Camera.main.GetComponent<LevelCreation>().canSwitch == true){

					if(randomNumber == 0){
						Camera.main.GetComponent<LevelCreation>().leveltypes = LevelCreation.LevelTypes.generic;
					}else if(randomNumber == 1){
						Camera.main.GetComponent<LevelCreation>().leveltypes = LevelCreation.LevelTypes.amoury;
					}else if(randomNumber == 2){
						Camera.main.GetComponent<LevelCreation>().leveltypes = LevelCreation.LevelTypes.medicalBay;
					}

					Camera.main.GetComponent<LevelCreation>().canSwitch = false;
					Camera.main.GetComponent<LevelCreation>().chunkSwitchTimer = 0;
				}
			}
		}
		
		//Spins the corner chunk to create illusion of going around corner 
		if(isCorner){
			if(cornerSpinLeft){
				if(isLeft){
					if(transform.eulerAngles.y < 270){
						transform.Rotate(0,(400 * Time.deltaTime),0);
					}
					if(transform.eulerAngles.y > 270){
						tempSpinAngles = transform.eulerAngles;
						tempSpinAngles.y = 270f;
						transform.eulerAngles = tempSpinAngles;
					}
				}
			}else if(cornerSpinRight){
				if(isRight){
					if(transform.eulerAngles.y > 90){
						transform.Rotate(0,(-400 * Time.deltaTime),0);
					}
					if(transform.eulerAngles.y < 90){
						tempSpinAngles = transform.eulerAngles;
						tempSpinAngles.y = 90f;
						transform.eulerAngles = tempSpinAngles;
					}
				}
			}
		}
		
		if(hasLights){
			light1.transform.Rotate(0,(600 * Time.deltaTime),0);
			light2.transform.Rotate(0,(600 * Time.deltaTime),0);	
		}
	}
}
