﻿using UnityEngine;
using System.Collections;

public class LevelCreation : MonoBehaviour {
	
	//Level Generation Variables
	private GameObject levelParent;
	private GameObject currentChunk;
	private GameObject previousChunk;
	private Vector3 snapPosition;
	private float snapOffset;
	private int currentLength;
	private int levelLength;
	private int addLoop;
	public Vector3 levelSpawnPoint; 
	
	//Powerup generation Variables
	private int ChunksSincePower;
	
	//Random chunk variables
	private string chunkToLoadName;
	private int randomNumber;
	private int randomEndingSection;
	
	//Level Behaviour variables
	private ArrayList levelChunks;
	private GameObject loopedChunk;
	private float deleteOffset;
	public float levelSpeed;
	private Vector3 spinTemp;
	[HideInInspector]
	public bool releaseAngles;
	
	// Use this for initialization
	private void Start () {
		
		levelParent = new GameObject("LevelChunks");
		levelParent.transform.tag = "levelChunks";
		levelLength = 4;
		deleteOffset = -28.0f;
		
		levelChunks = new ArrayList();
	}
	
	// Update is called once per frame
	private void Update () {
		
		GenerateLevel();
		LevelBehaviour();
	}
	
	private void GenerateLevel(){
		
	if(addLoop < 2){
		if(currentLength < levelLength){
			//Throw a random number to decide which chunk to load (only mid generation chunks).
			randomNumber = Random.Range(0,5);
			if(currentLength == 0){
					chunkToLoadName = "CorridorStraight";
				}else{
					if(ChunksSincePower == 15){
						chunkToLoadName = "CorridorStraightPowerUp";
						Camera.main.GetComponent<PowerUps>().justSpawned = true;
						ChunksSincePower = 0;
					}else{
						if(randomNumber == 0){
							chunkToLoadName = "CorridorStraight";
						}else if(randomNumber == 1){
							chunkToLoadName = "CorridorStraightLazers";
						}else if(randomNumber == 2){
							chunkToLoadName = "CorridorStraightLazers2";
						}else if(randomNumber == 3){
							chunkToLoadName = "CorridorStraightRoof";
						}else if(randomNumber == 4){
							if(Camera.main.GetComponent<PowerUps>().randomNumber == 1 && 
								Camera.main.GetComponent<PowerUps>().justSpawned == false){
								chunkToLoadName = "CorridorStraightPowerUp";
								Camera.main.GetComponent<PowerUps>().justSpawned = true;
								ChunksSincePower = 0;
							}else{
								chunkToLoadName = "CorridorStraight";
							}
						}
					}
				}
			}
	}else{
		randomEndingSection = Random.Range(0,3);
		
		if(randomEndingSection == 0){
			chunkToLoadName = "CorridorDoor";
		}else if(randomEndingSection == 1){
			chunkToLoadName = "CorridorCornerLeft";
		}else if(randomEndingSection == 2){
				chunkToLoadName = "CorridorCornerRight";
		}
	}
	
	//Choose the type of chunk to load, then add to array
		if(currentLength == 0){
		currentChunk = (GameObject)Instantiate(Resources.Load(chunkToLoadName), 
			           levelSpawnPoint,Quaternion.Euler(0,180,0));
				levelChunks.Add(currentChunk);
				ChunksSincePower ++;
		}else if(currentLength < levelLength){
			if(chunkToLoadName == "CorridorCornerLeft" || chunkToLoadName == "CorridorCornerRight"){
				currentChunk = (GameObject)Instantiate(Resources.Load(chunkToLoadName), 
				new Vector3(snapPosition.x,snapPosition.y,snapPosition.z),Quaternion.Euler(0,180,0));
				levelChunks.Add(currentChunk);
				
				currentChunk.transform.position = new Vector3(snapPosition.x,snapPosition.y,
					(snapPosition.z + Vector3.Distance(currentChunk.transform.position,
						currentChunk.transform.FindChild("StartLink").transform.position)));
					currentChunk.transform.FindChild("StartLink").renderer.enabled = false;
			}else{
				currentChunk = (GameObject)Instantiate(Resources.Load(chunkToLoadName), 
				new Vector3(snapPosition.x,snapPosition.y,snapPosition.z),Quaternion.Euler(0,180,0));
				levelChunks.Add(currentChunk);
				ChunksSincePower++;
			}
		}
		
		//make the chunk just loaded the previous chunk for chunk position snapping
		previousChunk = currentChunk;
		
		//Unrender the snap point
		previousChunk.transform.FindChild("EndLink").renderer.enabled = false;
		
		//Add the current chunk under the level parented object
		currentChunk.transform.parent = levelParent.transform;
		
		//As chunks are added ++ the current legnth of the level
		if(currentLength < levelLength){
			currentLength++;
			
			if(addLoop < 2){
				addLoop++;
			}else{
				addLoop = 0;	
			}
		}
	}

	private GameObject left;
	private GameObject right;
	private GameObject top;
	private GameObject bottom;
	public Material amouryMaterial;
	public Material medicalBayMaterial;

	[HideInInspector]
	public float chunkSwitchTimer;

	[HideInInspector]
	public bool canSwitch;

	[HideInInspector]
	public enum LevelTypes{
		generic,
		amoury,
		medicalBay,
	}

	[HideInInspector]
	public LevelTypes leveltypes = LevelTypes.generic;

	
	private void LevelBehaviour(){
		
		//Loop through the chunk array checking if they have gone offscreen, if so removed from chunk list,and destroy
		for(int i = 0; i < levelChunks.Count; i++){
			loopedChunk = (GameObject)levelChunks[i];
			
			if(loopedChunk.transform.position.z <= deleteOffset){
				if(loopedChunk.name == "CorridorCornerLeft(Clone)" || loopedChunk.name == "CorridorCornerRight(Clone)"){
					Camera.main.GetComponent<LevelCreation>().releaseAngles = false;
					Camera.main.GetComponent<Control>().lockControls = false;
				}
					levelChunks.Remove(loopedChunk);
					Destroy(loopedChunk);
					currentLength --;
			}
			
			if(Camera.main.GetComponent<Control>().GameStates == Control.gameStates.Playing){
				//Moves the level backwards towards the player using gametime
				loopedChunk.transform.position = new Vector3(loopedChunk.transform.position.x,
					loopedChunk.transform.position.y,loopedChunk.transform.position.z - (levelSpeed * Time.deltaTime));

				if(chunkSwitchTimer >= 5){
					canSwitch = true;
				}else{
					chunkSwitchTimer += Time.deltaTime;
				}

				if(loopedChunk.transform.GetComponent<ChunkBehaviour>().textureSet == false){

					//Set the gameObject of the walls
					left = loopedChunk.transform.FindChild("left").gameObject;
					right = loopedChunk.transform.FindChild("right").gameObject;
					top = loopedChunk.transform.FindChild("top").gameObject;
					bottom = loopedChunk.transform.FindChild("bottom").gameObject;

					//Switch materials to specific type
					if(leveltypes == LevelTypes.amoury){
						left.transform.renderer.material = amouryMaterial;
						right.transform.renderer.material = amouryMaterial;
						top.transform.renderer.material = amouryMaterial;
						bottom.transform.renderer.material = amouryMaterial;
					}else if(leveltypes == LevelTypes.medicalBay){
						left.transform.renderer.material = medicalBayMaterial;
						right.transform.renderer.material = medicalBayMaterial;
						top.transform.renderer.material = medicalBayMaterial;
						bottom.transform.renderer.material = medicalBayMaterial;
					}

					loopedChunk.transform.GetComponent<ChunkBehaviour>().textureSet = true;
				}
				
				//Maintains the correct spin rotation on all spawned chunks
				if(!releaseAngles){
					spinTemp = new Vector3(0,180,currentChunk.transform.eulerAngles.z);
					spinTemp.z = Camera.main.GetComponent<Control>().currentSpinAngle;
					loopedChunk.transform.eulerAngles = spinTemp;
				}
			}
			//Choose the snap position for the level chunk currently created
			snapPosition = previousChunk.transform.FindChild("EndLink").transform.position;	
		}
	}
}
