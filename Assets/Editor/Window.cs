﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class Window : EditorWindow
{
	private GameObject chunkInstance;
	private GameObject loadedChunkInstance;
	private string chunkToLoad;
	private string nameOfNewChunk;
	
	// Add menu item named "My Window" to the Window menu
	[MenuItem("Window/Level Editor")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(Window));
	}

	public void CreateNew(GameObject obj,string localPath) {
		//Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
		PrefabUtility.CreatePrefab("Assets/EditorChunks/" + chunkInstance.name + ".prefab",chunkInstance);
		//PrefabUtility.ReplacePrefab(obj, chunkInstance, ReplacePrefabOptions.ConnectToPrefab);
	}

	private void OnGUI()
	{
		//Title Label
		GUILayout.Label ("LevelEditor", EditorStyles.boldLabel);

		nameOfNewChunk = EditorGUI.TextField(new Rect(10,25,position.width - 20, 20),
		                                  "Name of new Chunk:", 
		                                     nameOfNewChunk);
		//Button to save the scetion  for loading
		GUILayout.Label(" ");
		GUILayout.Label(" ");
		if(GUILayout.Button("Create Chunk")){
			chunkInstance = (GameObject)Instantiate(Resources.Load("CorridorStraight"), 
				new Vector3(0,0,0),Quaternion.Euler(0,90,0));
			chunkInstance.name = nameOfNewChunk;
		}

		GUILayout.Label(" ");
		chunkToLoad = EditorGUI.TextField(new Rect(10,90,position.width - 20, 20),
		                               "Chunk to load:", 
		                                  chunkToLoad);
		GUILayout.Label(" ");
		if(GUILayout.Button("Load Chunk")){

			loadedChunkInstance = Resources.LoadAssetAtPath<GameObject>("Assets/EditorChunks/" + chunkToLoad + ".prefab");
			chunkInstance = (GameObject)Instantiate(loadedChunkInstance,new Vector3(0,0,0),Quaternion.Euler(0,90,0));
			chunkInstance.name = chunkToLoad;
		}

		GUILayout.Label(" ");
		if(GUILayout.Button("Save Chunk")){
			CreateNew(chunkInstance,"Assets/EditorChunks");
		}
	}
}